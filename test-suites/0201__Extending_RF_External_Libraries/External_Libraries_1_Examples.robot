*** Settings ***
Documentation  Example test suite for external libraries using Selenium library
...            (https://robotframework.org/Selenium2Library/Selenium2Library.html),
...            different test styles, custom robot keywords and setup/teardown.
Library  SeleniumLibrary
Library  OperatingSystem

Test Setup  Open Browser  ${url}  ${browser}
Test Teardown  Close Browser


*** Variables ***
${browser}  Chrome
${url}  http://automationpractice.com/index.php


*** Test Cases ***
Example
  Capture Page Screenshot    filename=selenium-screenshot-index.png