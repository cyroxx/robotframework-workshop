*** Settings ***
Documentation  This is the documentation of this test suite.
Library  BuiltIn
#Library  <Libraryname>

Test Setup  My Test Setup
Suite Setup  My Test Suite Setup
Test Teardown  My Test Teardown
Suite Teardown  My Test Suite Teardown


*** Variables ***
${my_var}  anything


*** Test Cases ***
Just Logging
  ${return}=  My Keyword  my
  Log  ${return}


*** Keywords ***
My Test Setup
  Log  Starting Test Case
  
My Test Teardown
  Log  Ending Test Case

My Test Suite Setup
  Log  Starting Test Suites

My Test Suite Teardown
  Log  Ending Test Suites

My Keyword
  [Arguments]  ${arg1}  ${arg_with_default}=value
  Log To Console  \n${arg1} ${arg_with_default}
  [Return]  Done